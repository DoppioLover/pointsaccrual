package sbux.ucp.points.accrual.services

import scala.concurrent.Future

/**
  * Project: Points Accrual
  * Author: honzhang on 11/21/2016.
  */
trait PointsAccrualService {
  def handleInsert(): Future[_]
}

class PointsAccrualServiceImpl extends PointsAccrualService{
  override def handleInsert(): Future[_] = ???
}

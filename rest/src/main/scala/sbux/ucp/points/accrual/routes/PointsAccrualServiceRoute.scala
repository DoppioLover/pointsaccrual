package sbux.ucp.points.accrual.routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import sbux.ucp.points.accrual.services.PointsAccrualService

import scala.util.{Failure, Success}

/**
  * Project: Points Accrual
  * Author: honzhang on 11/21/2016.
  */
class PointsAccrualServiceRoute(val service: PointsAccrualService) {
  import service._
  val route = pathPrefix("points") {
    insertRoute
  }

  def insertRoute: Route =  pathEndOrSingleSlash {
    post { // if there is nothing, we should reject it always
      onComplete(handleInsert()) {
        case Success(_) => complete( "Success" )
        case Failure(ex) => complete("Error:" + ex.getMessage)
      }
    }
  }
}

import NativePackagerHelper._

// -----------------------------
// resolvers (source repositories)
// -----------------------------
val sharedSettings: Seq[Def.Setting[_]] = Defaults.coreDefaultSettings ++ Seq(
  organization := "sbux.ucp",
  scalaVersion := "2.11.8",
  crossScalaVersions := Seq("2.10.6", "2.11.8"),
  resolvers ++= Seq(
    "Twitter Repository" at "http://maven.twttr.com",
    Resolver.jcenterRepo,
    Resolver.sonatypeRepo("releases"),
    Resolver.bintrayRepo("websudos", "oss-releases")
  ),
  scalacOptions in ThisBuild ++= Seq(
    "-language:_",
    "-Yinline-warnings",
    "-Xlint",
    "-deprecation",
    "-feature",
    "-unchecked"
  ),
  logLevel in ThisBuild := Level.Info,
  libraryDependencies ++= commonDependencies,
  fork in Test := true,
  javaOptions ++= Seq(
    "-Xmx1G",
    "-Djava.net.preferIPv4Stack=true",
    "-Dio.netty.resourceLeakDetection"
  ),
  //testFrameworks in PerformanceTest := Seq(new TestFramework("org.scalameter.ScalaMeterFramework")),
  //testOptions in Test := Seq(Tests.Filter(x => !performanceFilter(x))),
  //testOptions in PerformanceTest := Seq(Tests.Filter(x => performanceFilter(x))),
  //fork in PerformanceTest := false,
  parallelExecution in ThisBuild := false,
  mappings in Universal ++= contentOf((baseDirectory in LocalRootProject).value / "common-resource")
)

lazy val akkaVersion = "2.4.11"
lazy val akkaCirceVersion = "1.10.0"
lazy val catsVersion = "0.7.2"
lazy val shapelessVersion = "2.3.2"
lazy val refinedVersion = "0.5.0"
lazy val monixVersion = "2.0.0"
lazy val circeVersion = "0.6.0"
lazy val phantomVersion = "1.29.5"
lazy val logbackVersion = "1.1.3"
lazy val scalaParserVersion = "1.0.4"
lazy val scalatestVersion = "3.0.0-M9"
lazy val scalacheckVersion = "1.12.5"

lazy val compileOptions = Seq(
  "-unchecked",
  "-deprecation",
  "-language:_"
)

lazy val commonDependencies = Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "ch.qos.logback" % "logback-classic" % logbackVersion,
  "io.circe" %% "circe-core" % circeVersion,
  "org.scala-lang.modules" %% "scala-parser-combinators" % scalaParserVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,

  // for testing
  "org.scalatest" %% "scalatest" % scalatestVersion,
  "org.scalacheck" %% "scalacheck" % scalacheckVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion
)

lazy val phantomDependencies = Seq(
  "com.websudos" %% "phantom-dsl" % phantomVersion,
  "com.websudos" %% "phantom-reactivestreams" % phantomVersion,
  "com.outworkers" %% "util-testing" % "0.18.2" % Test
)

lazy val httpDependencies = Seq(
  "com.typesafe.akka" %% "akka-http-core" % akkaVersion,
  "com.typesafe.akka" %% "akka-http-experimental" % akkaVersion,
  "de.heikoseeberger" %% "akka-http-circe" % "1.10.1",

  "com.typesafe.akka" %% "akka-http-testkit" % akkaVersion
)

lazy val akkaStreamDependencies = Seq(
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream-kafka" % "0.13",
  "org.apache.kafka" %% "kafka" % "0.10.0.1",
  "org.apache.kafka" % "kafka-clients" % "0.10.0.1"
)

lazy val rest = Project(id = "points-accrual-rest", base = file("rest"))
  .settings(sharedSettings: _*)
  .dependsOn(events).dependsOn(api)
  .enablePlugins(AshScriptPlugin)
  .enablePlugins(DockerPlugin)
  .settings(libraryDependencies ++= httpDependencies)
  .settings(
    name := "points accrual rest"
  )

lazy val events = Project(id = "points-accrual-events", base = file("events"))
  .settings(sharedSettings: _*)
  .enablePlugins(AshScriptPlugin)
  .settings(
    name := "points accrual events"
  )

lazy val api = Project(id = "points-accrual-api", base = file("api"))
  .settings(sharedSettings: _*)
  .enablePlugins(AshScriptPlugin)
  .settings(
    name := "points accrual api"
  )

lazy val repo = Project(id = "points-accrual-repo", base = file("repo"))
  .dependsOn(api)
  .settings(sharedSettings: _*)
  .enablePlugins(AshScriptPlugin)
  .settings(libraryDependencies ++= phantomDependencies)
  .settings(
    name := "points accrual repo"
  )

lazy val producer = Project(id = "points-accrual-producer", base = file("logic/points-accrual-producer"))
  .dependsOn(events)
  .enablePlugins(AshScriptPlugin)
  .enablePlugins(DockerPlugin)
  .settings(sharedSettings: _*)
  .settings(libraryDependencies ++= akkaStreamDependencies)
  .settings(
    name := "points accrual producer"
  )

lazy val root = Project(id = "points-accrual-root", base = file("."))
  .aggregate(events, producer, events, api, repo)
  .settings(
    name := "Points Accrual",
    version := "0.1"
  )


